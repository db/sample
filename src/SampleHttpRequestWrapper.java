import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


public class SampleHttpRequestWrapper extends HttpServletRequestWrapper {
	
	private String wrappedHeaderValue;

	public SampleHttpRequestWrapper(HttpServletRequest request, String wrappedHeaderValue) {
		super(request);
		this.wrappedHeaderValue = wrappedHeaderValue;
	}

	@Override
	public String getHeader(String name) {
		if(name!=null && name.equals(Constants.USER_NAME_HEADER)){
			return this.wrappedHeaderValue;
		}
		return super.getHeader(name);
	}
}
